<?php

error_reporting(0);
require './config.inc.php';
require './connect.php';


$result = "ok";
$err_no = 0;

$oper = $_POST['oper'];

if (empty($oper)) {
    $err = array('result' => 'empty',val => NULL);
    errorOut($err);
    return;
}

$dbpars = $_POST['dbpars'];
$dbname = $dbpars['dbname'];
$tablename = $dbpars['tablename'];
$usquery = $_POST['query'];

if ($oper == 'selectquery') {
    if (empty($usquery) || empty($dbname)) {
	$err = array('result' => 'not query',val => NULL);
	errorOut($err);
	return;
    }
} else if (empty($oper) || empty($dbname) || empty($tablename)) {
    $err = array('result' => 'not dbparameters',val => NULL);
    errorOut($err);
    return;
}

if (!mysqli_select_db($link, $dbname)) {
    $err_no = mysqli_errno($link);
    $result = mysqli_error($link);
    $err = array('err_no' => $err_no, 'result' => $result,val => NULL);
    errorOut($err);
    return;
}

// вывести все записи из таблицы
if ($oper == 'listall') {
    $select = "SELECT * FROM " . $tablename;
    $query = $select;
    $res = mysqli_query($link, $query);
}
// вывести все записи из таблицы  для user
// имя ячейки должно быть при этом user (!!!!)
elseif ($oper == 'listallforuser') {
    $user = $_POST['user'];
    if (empty($user)) {
	$err = array('result' => 'not parameter user',val => NULL);
	errorOut($err);
	return;
    }
    $select = "SELECT * FROM " . $tablename . " WHERE user='" . $user . "'";
    $query = $select;
    $res = mysqli_query($link, $query);
}
// количество записей в таблице
elseif ($oper == 'countall') {
    $select = "SELECT COUNT(1) FROM " . $tablename;
    $query = $select;
    $res = mysqli_query($link, $query);
}
// произвольный запрос для SELECT
elseif ($oper == 'selectquery') {
    $query = "SELECT " . $usquery;
    $res = mysqli_query($link, $query);
}
// вставить новую запись
// для пользования этой операцией, последовательность полей должна строго быть соблюдена
// записываются все столбцы, кроме первого. Первый это id, он обозначен как AUTO_INCREMENT
elseif ( $oper == 'insertnew') {
    $params = $_POST['params'];
    if (empty($params)) {
	$err = array('result' => 'not parameters',val => NULL);
	errorOut($err);
	return;
    }
    $n = count($params);
    
    $aus = "";
    for ($i=0; $i < $n; $i++) {
	if($i) $aus = $aus . ",";
	$aus = $aus . "'" . $params[$i] . "'";
    }
    $query = "INSERT INTO " . $tablename . " VALUES( default, " . $aus . ")";
    $res = mysqli_query($link, $query);
}

if (!$res) {
    $err_no = mysqli_errno($link);
    $result = mysqli_error($link);
    $out = NULL;
} else {
    if ($oper == 'countall') {
	//result = mysql_store_result(connection);
	$ret = mysqli_fetch_row($res);
	$out[] = $ret[0];
    } elseif ($oper == 'insertnew') {
	$out = "success";
    }
    else {
	while ($ret = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
	    $out[] = $ret;
	};
    }
}

$ret = array('err_no' => $err_no, 'result' => $result, 'query' => $query, 'val' => $out);
$jsen = json_encode($ret);
echo $jsen;
mysqli_close($link);
?>

<?php

//FUNCTION
function errorOut($outres) {
    global $link;
    $jsen = json_encode($outres);
    echo $jsen;
    mysqli_close($link);
}
?>

